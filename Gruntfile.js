/**
 *
 *
 * Created by Rockerz on 21/12/14.
 */
module.exports = function (grunt){

    grunt.initConfig({
        uglify: {
            my_target: {
                options:{
                    sourceMap:true
                },
                files: {
                    'build/rockerz.utlis.min.js': ['build/rockerz.utils.js']
                }
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['src/*.js'],
                dest: 'build/rockerz.utils.js'
            }
        },
        watch: {
            scripts: {
                files: 'src/*.js',
                tasks: ['concat','uglify'],
                options:{

                    event:['changed','added']

                }

            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');


}