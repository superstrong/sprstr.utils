(function (global){
    'use strict'

    var rockerz = rockerz ||{};
    rockerz.NumberUtils = {};

    var c = rockerz.NumberUtils;

    /**
     * Return an random number between min and max
     * @param min
     * @param max
     * @param floor
     * @returns {*}
     */
    c.randomRange = function (min, max, floor){

        if (floor){

            return (Math.floor(Math.random() * (max - min + 1)) + min);

        }else {

            return Math.random() * (max - min + 1) + min;

        }

    };

    /**
     * Convert a number into radiant
     * @param value
     * @returns {number}
     */
    c.toRadiants = function (value) {

        return (Math.PI*value) / 180

    };

    /**
     * Convert a nmuber into degree
     * @param value
     * @returns {number}
     */
    c.toDegrees = function (value) {

        return (value * 180) / Math.PI

    };

    /**
     * Compute the angle based on 2 point (start and end)
     * @param startX
     * @param startY
     * @param endX
     * @param endY
     * @returns {number}
     */
    c.pointsToAngle = function (startX,startY,endX,endY){

        var deltaX,
            deltaY;
        deltaX = endX - startX;
        deltaY = endY - startY;
        return Math.atan2(deltaY,deltaX);

    }

    global.rockerz = rockerz

})(this);